import { useMediaQuery } from '@vueuse/core'

export const isPhone = useMediaQuery('(max-width: 500px)')

export const makeCredentialOptions = async({challenge, id, email, name}) => {
  return {
    challenge: Uint8Array.from(challenge, c => c.charCodeAt(0)),
    rp: {
      name: "Proposal for federated cooperation",
      id: "skinnyjames.net"
    },
    user: {
      id: Uint8Array.from(id, c => c.charCodeAt(0)),
      name: email,
      displayName: name 
    },
    pubKeyCredParams: [{alg: -7, type: "public-key"}],
    authenticatorSelection: {
      authenticatorAttachment: "cross-platform"
    },
    timeout: 60000,
    attestation: none
  }
}

export const getCredential = async() => {
  const credential = await navigator.credentials.create({
    publicKey: publicKeyCredentialCreationOptions
  })

  return credential
}