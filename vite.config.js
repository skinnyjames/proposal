import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Markdown from 'vite-plugin-vue-markdown'
import MarkdownItAnchor from 'markdown-it-anchor'

export default defineConfig({
  plugins: [
    vue({
      include: [/\.vue$/, /\.md$/]
    }),
    Markdown({ 
      markdownItOptions: { linkify: true, html: true }, 
      markdownItSetup(md) { 
        md.use(MarkdownItAnchor, { level: [1,2,3] })
      }
    })
  ],
})
