require "sequel"
require "rest-client"
require 'json'

counties = JSON.parse(RestClient.get('https://geoffrey.skinnyjames.net/api/v1/us/counties').body)

DB = Sequel.sqlite('test-proposal.db')

40_000.times do |i|
  name = "Josh #{i}."
  interest_type = ['coop', 'start', 'work'].sample
  county_id = counties.sample['id']
  comment = <<-EOF
  This is really a good thing for me.
  EOF
  DB[:signatures].insert(name: name, interest_type: interest_type, county_id: county_id, comment: comment)
end