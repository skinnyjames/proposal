require "sinatra"
require "sinatra/json"
require "sequel"
require "rest-client"
require "uri"

database = ENV['PROPOSAL_ENV'] == 'test' ? 'test-proposal.db' : 'proposal.db'

DB = Sequel.sqlite(database)

unless DB.table_exists?(:signatures)
  DB.create_table :signatures do
    primary_key :id

    column :name, String
    column :interest_type, String
    column :county_id, Integer
    column :comment, String
    column :verified, [TrueClass, FalseClass], default: false
  end
end

GEOFFREY_API = ENV.fetch("GEOFFREY_API", "http://localhost:3000/api/v1")

set :public_folder, "#{__dir__}/dist"
set :logging, false

before do
  if request.body.size > 0
    request.body.rewind
    @request_payload = JSON.parse(request.body.read, symbolize_names: true)
  end
end

get "/" do
  send_file "#{__dir__}/dist/index.html"
end

get "/api/states" do
  RestClient.get("#{GEOFFREY_API}/us/states").body
end

get "/api/states/:id/counties" do |id|
  RestClient.get("#{GEOFFREY_API}/us/states/#{id}/counties").body
end

get "/api/counties" do
  RestClient.get("#{GEOFFREY_API}/us/counties").body
end

get "/api/county/:id/signatures" do |id|
  json DB[:signatures].where(county_id: id).all.slice(:name, :interest_type, :comment, :county_id)
end

get '/api/interest-stats' do
  res = DB['select count(s.id) as count, s.interest_type from signatures as s group by s.interest_type'].all.reduce({}) do |memo, thing|
    memo[thing[:interest_type]] = thing[:count]
    memo
  end
  json res
end

get '/api/signatures' do
  id = params[:county_id]

  if id && !id.empty?
    json DB['select name, comment, interest_type from signatures where county_id = ? order by RANDOM() limit 5', id].all
  else
    json DB['select name, comment, interest_type from signatures order by RANDOM() limit 5'].all
  end
end

get "/api/stats" do
  records = []

  counties = JSON.parse(RestClient.get("#{GEOFFREY_API}/us/counties")).reduce({}) do |memo, entry|
    memo[entry['id']] = entry
    memo
  end

  DB['select count(s.id) as count, s.county_id as county_id from signatures as s group by s.county_id'].each do |row|
    row[:county_name] = counties[row[:county_id]]&.[]('name')
    begin
      row[:geom] = JSON.parse(counties[row[:county_id]]&.[]('geom'))
    rescue => e
      row[:geom] = nil
    end

    unless row[:geom].nil? || row[:county_id].nil?
      records << { county_name: row[:county_name], geom: row[:geom], count: row[:count], county_id: row[:county_id] }
    end
  end

  json records
end

post "/api/sign" do
  if @request_payload[:email]
    halt(200)
  end

  has_fields = [:name, :interest_type, :county_id].reduce(true) do |memo, key|
    memo && @request_payload[key]
  end

  halt(400, "Please fill out all required fields.") unless has_fields
  halt(400, "Incorrect fields detected") if !['coop', 'start', 'work'].include?(@request_payload[:interest_type])

  counties = JSON.parse(RestClient.get("#{GEOFFREY_API}/us/counties").body)
  found = counties.find { |county| county['id'] == @request_payload[:county_id] }
  halt(400, "County cannot be found.  Something probably wen't wrong.") unless found

  begin
    hash = @request_payload.slice(:name, :interest_type, :county_id, :comment)

    if !hash[:comment].nil? && !hash[:comment].empty?
      hash[:comment] = hash[:comment][0...280] if (hash[:comment].size > 280)
      hash[:comment].gsub!(/#{URI::regexp}/, '')
    end

    hash[:name] = hash[:name][0...70]

    DB[:signatures].insert(**hash)
  rescue StandardError => ex
    halt(500, "An error occurred: #{ex}")
  end

  halt(200)
end

 