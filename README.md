# Proposal for a federated cooperative

## Specs and Ethics

Please see `files/spec.md` and `files/ethics.md`

## Contributing

Please make a PR!