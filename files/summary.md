A cooperative is an autonomous association of persons united voluntarily to meet their common economic, social and cultural needs and aspirations through a jointly owned and democratically-controlled enterprise. Cooperative efforts operate in accordance with [:seven core principles](https://en.wikipedia.org/wiki/Rochdale_Principles)

This proposal takes the principle of _cooperation between cooperatives_ as a point of departure for reconsidering the ways in which we **provide value** and **satisfy members' needs**
as both individuals and communities

By working together on the efforts that allow us to thrive, we can meet our needs in a meaningful, efficient, and equitable way.

## Note

The geospatial portion of this proposal is currently only maintained for the United States.
If you live in another country and also align with this proposal, that's great!  

Please select District of Columbia as the state and county until this proposal can support other countries.
You can also write your location in the comment if you wish.

If you encounter any issues with the application part of this site, please email zero@skinnyjames.net.