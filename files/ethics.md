# Code of Ethics

## Preface

Oppression is a systemic condition of injustice that is embedded into the fabric of our society via the way we interact with each other and codify those interactions into systems. 
Oppression can be violent and extreme, such as colonialism, genocide or slavery.  Oppression can also be more subtle or not physically violent, such as hateful remarks that target a group of people.

The proposed code of ethics attempts to consider oppression in all of its forms from the onset of the effort.

It is informed by the hypothesis that group formation necessitates abstract or archetypal "chosen" criteria for membership, 
which simultaneously imposes "negative" criteria on non-members.  Through the mechanics of imposition, group purity embeds a relationship of oppression and power between the explicit group and the implicit group of those who don't belong.  

When group membership is systematically encoded into social identity, oppression is embedded in the very way we interface with each other.  
When this oppression is approached by accepting or forming new group identities, it not only cements the original criteria for oppression, but also fabricates new ones by the same mechanic of imposition. 

This segmentation of social identity into telescoping groups has vicious effects:

* Dehumanization occurs when abstract groups are privileged over individual humans
* Segregation occurs when individuals don't share any common groups.
* The individual's sense of self is fragmented on the intersections of their memberships.

Thus, we must explicitly consider and address oppression using an ethical framework that serves individual bodies over group identities.

## Specification

In order to shed group identity there first needs to be a superordinate protocol for ethics in place that is effective in providing safe phsyical and psychological space.

### I. Protect peers from imposed grouping using the redistribution of belonging

If an individual imposes a group identity onto a peer verbally or non-verbally, intentionally or not, it psychologically separates the peer into a fragmented group.
This mechanic is called "othering."  Since the individual cannot push back solely on their own without reifying the criteria for separation, it is imperative that we affirm and redistribute
an explicit sense of belonging by speaking up immediately.

Speaking up immediately restributes the sense of belonging into those that share this criteria for ethics and those that don't.

### II. Protect peers from non-consensual violence.

Aside from having a significant and severe impact on victims, violence also has a corrosive effect on humanity as a whole, and severely limits the ability for an organized effort to provide value.

### III. Assume best intent

Using Nicky Case's visualization of the <a href="https://ncase.me/trust" target="_blank">evolution of trust</a> during repeated encounters in a zero sum game,
the [:Copykitten](https://ncase.me/trust/notes#CopyKitten) strategy demonstrates a critical form of cooperation that allows room for mistakes and misunderstandings.

By **assuming best intent** and **asking for genuine clarification** sustained conversations can evaluate and be fixed on strategy toward an ethical result as opposed to personal conflict.

## Note on yielding

Sometimes discerning the relation of a choice or strategy to an ethical framework takes time. In these cases, it may be better to yield to a normative or popular ethics until the relation can be properly described and understood.

## Derived values

When addressing oppression on an individual level, corollary value systems might result in:

* Approaching systemic oppression and privilege as associated with characteristics instead of identities
* Privileging diversity defined as different values, perspectives and strategies
* Rejecting grouping as ethical leverage for or against an argument

<a href="https://codeberg.org/skinnyjames/proposal/_edit/main/files/ethics.md" target="_blank">Propose changes to these ethics</a>