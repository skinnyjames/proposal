# Specification

This specification for federated cooperation intends to provide a working scaffold for scaling cooperative efforts efficiently and sustainably. They are expressed as a series of intentions that should always be measured up against actual results in order to evaluate their value.  Bear in mind that this is a living document, and subject to change.

## I. Intention to provide value

This proposal asserts that our socio-economic conditions are downstream from _our own_ efforts, and that we are accountable for these conditions via the manner in which we provide value.  

The value that these efforts provide is distinct from any signifiers such as currency.  The aim of a federated cooperative effort should first and foremost be to provide value through meaningful work.

## II. Intention to provide ability to provide different kinds of value

This proposal asserts that value is based on **need**, and that sometimes a supply of value isn't always needed.  An aim of a federated cooperative should be to foster the ability of its workers to move to where the need is.

Privileging ability/mobility is in accordance with the cooperative principle of education, training, and information.

## III. Intention to remove dependencies on competitive efforts

This proposal asserts that solutions to problems are embedded with intention.  Reliance on efforts that are systemically for profit limits our ability to provide solutions that aren't in the service of profiting off of each other.  Directing effort toward self-reliance and reconsidering "common" solutions is encouraged.

## VI. Intention toward efficiency

This proposal asserts that truly efficient efforts are also the most sustainable.  Since the goal of a federated cooperative is not to provide _jobs_ but rather _means to add value_, a federated cooperative should not be afraid of removing the need for work.  If there is no need for work, then members should have their needs met already without purposeless activity.

<a href="https://codeberg.org/skinnyjames/proposal/_edit/main/files/spec.md" target="_blank">Propose changes to this spec</a>